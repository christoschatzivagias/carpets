import React from 'react'
import { Row, Col, Container, Form, Button } from 'react-bootstrap';
import { searchCustomer, getCustomers, deleteCustomer } from './UtilityFunctions';

class CustomerTable extends React.Component {
    constructor(pros){
        super();
        this.state = {
           results: []
        };
    }

    delete(id) {
        deleteCustomer(id).then(result=>{
            getCustomers().then((results) => {
                this.setState({
                    results: results
                });
            });
        })
    }

    componentDidMount() {
        if(this.props.match.params.query !== undefined) {
            searchCustomer(this.props.match.params.query).then((results) => {
                this.setState({
                    results: results
                });
            });
        } else {
            getCustomers().then((results) => {
                this.setState({
                    results: results
                });
            });
        }
       
    }

   
    render() {
        const {results} = this.state

        return (
            <div>
                <Container fluid>
                    <Row key="title">
                        <Col key={1}><strong>Επώνυμο</strong></Col>
                        <Col key={2}><strong>Όνομα</strong></Col>
                        <Col key={3}><strong>Οδός</strong></Col>
                        <Col key={4}><strong>Περιοχή</strong></Col>
                        <Col key={5}><strong>Τηλέφωνο</strong></Col>
                        <Col key={6}><strong>Κινητό</strong></Col>
                        <Col key={7}><strong>Κινητό2</strong></Col>
                        <Col key={8}><strong>Α.Φ.Μ.</strong></Col>
                        <Col key={9}></Col>
                        <Col key={10}></Col>
                        <Col key={11}></Col>

                    </Row>
                    {
                      results.map((result, index) =>
                        <Row key={index}>
                            <Col><Form.Label>{result.surname}</Form.Label></Col>
                            <Col><Form.Label>{result.name}</Form.Label></Col>
                            <Col><Form.Label>{result.street}</Form.Label></Col>
                            <Col><Form.Label>{result.region}</Form.Label></Col>
                            <Col><Form.Label>{result.telephone}</Form.Label></Col>
                            <Col><Form.Label>{result.cellphone}</Form.Label></Col>
                            <Col><Form.Label>{result.cellphone2}</Form.Label></Col>
                            <Col><Form.Label>{result.vatid}</Form.Label></Col>
                            <Col><Form.Label><Button variant="link" href={'/carpetdelivery/' + result.id}>Παράδοση</Button></Form.Label></Col>
                            <Col><Form.Label><Button variant="link" href={'/carpetpickup/' + result.id}>Παραλαβή</Button></Form.Label></Col>
                            <Col><Form.Label><Button variant="link" onClick={this.delete.bind(this,result.id)}>Διαγραφή</Button></Form.Label></Col>
                        </Row>
                      )
                    }
                </Container>

            </div>

        )
  }
}

export default CustomerTable;
