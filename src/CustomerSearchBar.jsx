import React, { Component } from "react";
import { Form, FormControl, Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

class SearchBar extends Component {
  constructor() {
    super();
    this.state = {
      searchText: "",
    };
  }

  handleSearchTextChange = (event) => {
    this.setState({
      searchText: event.target.value
        .split(" ")
        .filter((s) => s.length > 0)
        .join("+"),
    });
  };

  render() {
    return (
      <div>
        <Form inline>
          <FormControl
            type="text"
            placeholder="Αναζήτηση Πελάτη"
            onChange={this.handleSearchTextChange.bind(this)}
          />
          <Button
            style={{margin: "2px"}}
            href={"/search/" + this.state.searchText}
          >
            Αναζήτηση
          </Button>
        </Form>
      </div>
    );
  }
}

export default SearchBar;
