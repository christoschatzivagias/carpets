import axios from "axios";

const base_url = "http://localhost:8080/api/v1"

export const registerCustomer = async (
  surname,
  name,
  street,
  floor,
  region,
  postalcode,
  telephone,
  cellphone,
  cellphone2,
  vatid,
  profession,
  taxregion) => {
  return axios.post(base_url + "/customer/register", {
    surname:surname,
    name:name,
    street:street,
    floor:floor,
    region:region,
    postalcode:postalcode,
    telephone:telephone,
    cellphone:cellphone,
    cellphone2:cellphone2,
    vatid:vatid,
    profession:profession,
    taxregion:taxregion
  })
  .then((response) => {
    return response.status;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });

}

export const searchCustomer = async (query) => {
  return axios
    .get(base_url + "/search/" + query)
    .then((response) => {
      return response.data;
    })
    .catch((err) => {
      console.error(err.message);
      throw err;
    });
};

export const registerCarpet = async(
  priceperunit,
  pricetype,
  typedescription, id, customerid, length, width, weight, extent, price, extraprice, pricevat, pickupid, pickupdate, notes) => {
  return axios.post(base_url + "/carpet/register", {
    priceperunit: priceperunit,
    pricetype: pricetype,
    typedescription: typedescription,
    id: id,
    customerid: customerid,
    length: length,
    width: width,
    weight: weight,
    extent: extent,
    price: price,
    pricevat: pricevat,
    extraprice: extraprice,
    pickupid: pickupid,
    pickupdate: pickupdate,
    notes: notes
  })
  .then((response) => {
    return response.status;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}


export const updateCarpet = async(
  priceperunit,
  pricetype,
  typedescription,
  id,
  customerid,
  length,
  width,
  weight,
  extent,
  price,
  pricevat,
  extraprice,
  notes,
  pickupid,
  pickupdate,
  deliverid,
  deliverdate,
  shelve) => {
  return axios.post(base_url + "/carpet/update", {
    priceperunit: priceperunit,
    pricetype: pricetype,
    typedescription: typedescription,
    id: id,
    customerid: customerid,
    length: length,
    width: width,
    weight: weight,
    extent: extent,
    price: price,
    pricevat: pricevat,
    extraprice: extraprice,
    notes: notes,
    pickupid: pickupid,
    pickupdate: pickupdate,
    deliverid: deliverid,
    deliverdate: deliverdate,
    shelve: shelve
  })
  .then((response) => {
    return response.status;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}


export const deliverCarpet = async(id, deliverid, deliverdate) => {
  return axios.post(base_url + "/carpet/deliver/" + id, {
    deliverid: deliverid,
    deliverdate: deliverdate
  })
  .then((response) => {
    return response.status;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}

export const getCarpetsByCustomerId = async (customerid) => {
  return axios.get(base_url + "/carpet/getbycustomer/" + customerid)
  .then((response) => {
    return response.data;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}

export const getCarpetById = async (carpetid) => {
  return axios.get(base_url + "/carpet/getbyid/" + carpetid)
  .then((response) => {
    return response.data;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}

export const getCarpetsByShelveId = async (shelveid) => {
  return axios.get(base_url + "/shelve/get/" + shelveid)
  .then((response) => {
    return response.data;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}

export const registerParameter = async (description, type, price)=> {
  return axios.post(base_url + "/parameter/register", {
    description: description,
    type: type,
    price: price
  })
  .then((response) => {
    return response.status;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}

export const deleteParameter  = async (paramid)=> {
  return axios.post(base_url + "/parameter/delete/" + paramid)
  .then((response) => {
    return response.status;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}
export const getParameters = async ()=> {
  return axios.get(base_url + "/parameter/get")
  .then((response) => {
    return response.data;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}

export const getCustomerById = async (id)=> {
  return axios.get(base_url + "/customer/get/" + id)
  .then((response) => {
    return response.data;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}

export const getVat = async ()=> {
  return axios.get(base_url + "/vat")
  .then((response) => {
    return response.data;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}

export const setVat = async(vat)=> {
  return axios.post(base_url + "/vat", {'vat': parseInt(vat)})
  .then((response) => {
    return response.status;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}

export const getCustomers = async ()=> {
  return axios.get(base_url + "/customers")
  .then((response) => {
    return response.data;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}


export const deleteCustomer = async (id)=> {
  return axios.post(base_url + "/customer/delete/" + id)
  .then((response) => {
    return response.data;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}

export const deleteCarpet = async (id)=> {
  return axios.post(base_url + "/carpet/delete/" + id)
  .then((response) => {
    return response.data;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}


export const getYearReport = async() => {
  const currentYear = new Date().getFullYear(); 
  return axios.get(base_url + "/report/" + currentYear)
  .then((response) => {
    return response.data
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}