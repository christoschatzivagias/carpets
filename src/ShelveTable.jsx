import React from 'react'
import { Row, Col, Container } from 'react-bootstrap';

class ShelveTable extends React.Component {
    constructor(){
        super();
        this.state = {
            shelvechars: ['Α','Β','Γ','Δ','Ε','Ζ','Η','Θ','Ζ','Ι','Κ','Λ','Μ','Ν','Ξ','Ο','Π','Ρ','Σ','Τ','Υ','Φ','Χ','Ψ','Ω'],
            shelvenumbers: [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
        }
    }

    render() {
        const {shelvechars, shelvenumbers} = this.state
        return (
            <div>
                <h2>Ράφια</h2>
                <Container fluid>
                    {
                        shelvechars.map((shelvechar, index)=>{
                            return (
                                <Row key={index}>
                                {
                                    shelvenumbers.map(shelvenumber=>(
                                        <Col key={shelvechar + shelvenumber}><a href={"/shelve/" + shelvechar + shelvenumber}>{shelvechar}{shelvenumber}</a></Col>
                                    ))
                                }
                                </Row>
                            )
                        }
                        )
                    }
                </Container>
            </div>
        )
  }
}

export default ShelveTable;
