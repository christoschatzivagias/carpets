import React from 'react';
import { Row, Col, Container, Button } from 'react-bootstrap';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import ServerMessage from './ServerMessage'


import {registerCustomer} from './UtilityFunctions';

class Comments extends React.Component {
  constructor(){
    super();
    this.state = {
      surname:"",
      name:"",
      street:"",
      floor:"",
      region:"ΚΑΤΕΡΙΝΗ",
      postalcode:"60100",
      telephone:"",
      cellphone:"",
      cellphone2:"",
      vatid:"",
      profession:"ΙΔΙΩΤΗΣ",
      taxregion:"ΚΑΤΕΡΙΝΗΣ",
      status: 0
    };
    this.numeric_fields=[
      "floor",
      "postalcode",
      "telephone",
      "cellphone",
      "cellphone2",
      "vatid"
    ]
  }

  submitButtonActive() {
    return this.state.surname.length > 0 && this.state.name.length > 0 && this.state.street.length > 0 &&
          this.state.region.length > 0 && this.state.postalcode.length > 0 &&
          (this.state.telephone.length > 0 || this.state.cellphone.length > 0 || this.state.cellphone2.length > 0);
  }

  handleChange(e){
    if(this.numeric_fields.includes(e.target.id)){
      const re = /^[0-9\b]+$/;
      if (e.target.value === '' || re.test(e.target.value)) {
        this.setState({[e.target.id]: e.target.value})
      }
    } else {
      this.setState({[e.target.id]: e.target.value})
    }
  }

  register(){
    registerCustomer(this.state.surname, this.state.name, this.state.street, this.state.floor,
            this.state.region, this.state.postalcode, this.state.telephone, this.state.cellphone,
            this.state.cellphone2, this.state.vatid,this.state.profession,this.state.taxregion)
            .then(status=>{
              this.setState({
                status:status
              })
            }).catch(()=>{
              this.setState({
                status:403
              })
            })
  }


  render() {
    return (
      <div>
        <h2>
        Στοιχεία Πελάτη
        </h2>
        <Container fluid="md">
          <Row>
            <Col sm>
              Επώνυμο
              <input id="surname" plaintext="true" value={this.state.surname} onChange={(e) => this.handleChange(e)}/>
            </Col>
            <Col sm>
              Όνομα
              <input id="name" plaintext="true" value={this.state.name} onChange={(e) => this.handleChange(e)}/>
            </Col>
          </Row>
          <Row>
            <Col sm>
              Οδός
              <input id="street" plaintext="true" value={this.state.street} onChange={(e) => this.handleChange(e)}/>
            </Col>
            <Col sm>
              Όροφος
              <input id="floor" plaintext="true" value={this.state.floor} onChange={(e) => this.handleChange(e)}/>
            </Col>
          </Row>
          <Row>
            <Col sm>
              Περιοχή
              <input id="region" plaintext="true" value={this.state.region} onChange={(e) => this.handleChange(e)}/>
            </Col>
            <Col sm>
              Τάχ.Κώδικας
              <input id="postalcode" plaintext="true" value={this.state.postalcode} onChange={(e) => this.handleChange(e)}/>
            </Col>
          </Row>
          <Row>
            <Col sm>
              Τηλέφωνο
              <input id="telephone" plaintext="true" value={this.state.telephone} onChange={(e) => this.handleChange(e)}/>
            </Col>
            <Col sm>
              Κινητό
              <input id="cellphone" plaintext="true" value={this.state.cellphone} onChange={(e) => this.handleChange(e)}/>
            </Col>
          </Row>
          <Row>
            <Col sm>
              Κινητό 2
              <input id="cellphone2" plaintext="true" value={this.state.cellphone2} onChange={(e) => this.handleChange(e)}/>
            </Col>
            <Col sm>
              Α.Φ.Μ.
              <input id="vatid" plaintext="true" value={this.state.vatid} onChange={(e) => this.handleChange(e)}/>
            </Col>
          </Row>
          <Row>
            <Col sm>
              Επάγγελμα
              <input id="profession" plaintext="true" value={this.state.profession} onChange={(e) => this.handleChange(e)}/>
            </Col>
            <Col sm>
              Δ.Ο.Υ.
              <input id="taxregion" plaintext="true" value={this.state.taxregion} onChange={(e) => this.handleChange(e)}/>
            </Col>

          </Row>
        </Container>
        <Button variant="primary" onClick={()=>{this.register()}} type="submit" disabled={!this.submitButtonActive()}>
         Καταχώρηση Πελάτη
        </Button>


        <ServerMessage show={this.state.status === 201} redirect={'/'} title='Επιτυχής!' message='Ο πελάτης καταχωρήθηκε επιτυχώς!'/>
        <ServerMessage show={this.state.status === 403} redirect={'/newcustomer'} title='Λάθος!' message='Ο πελάτης δεν καταχωρήθηκε'/>

    </div>
    )
  }
}

export default Comments;
