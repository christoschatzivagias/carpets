import React, { Component } from "react";
import { Nav, Navbar, Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { withRouter } from "react-router-dom";
import CustomerSearchBar from "./CustomerSearchBar";
import CarpetSearchBar from "./CarpetSearchBar";

class NavComp extends Component {
  handleSearchTextChange(event) {
    this.setState({
      searchText: event.target.value,
    });

    this.props.handleSearchTextChange(event);
  }

  handleSearchClick = () => {
    this.props.handleSearchClick();
  };

  render() {
    return (
      <Navbar bg="light" expand="lg">
        <Nav.Link href="/">Αρχική</Nav.Link>
        <Navbar.Brand href="/newcustomer">
          <Button>Καταχώρηση Πελάτη</Button>
        </Navbar.Brand>
        <Navbar.Brand href="/shelvetable">
          <Button>Ράφια</Button>
        </Navbar.Brand>
        <Navbar.Brand href="/parameters">
          <Button>Παράμετροι</Button>
        </Navbar.Brand>
        <Navbar.Brand href="/customers">
          <Button>Λίστα Πέλατων</Button>
        </Navbar.Brand>
        <Navbar.Collapse className="justify-content-end">
          <Nav pullright="true">
            <CarpetSearchBar />
          </Nav>
        </Navbar.Collapse>
        <Navbar.Collapse className="justify-content-end">
          <Nav pullright="true">
            <CustomerSearchBar />
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default withRouter(NavComp);
