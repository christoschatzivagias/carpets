import React from 'react'
import { Row, Col, Container, Button, Form } from 'react-bootstrap';
import { getParameters, registerParameter, getVat, setVat, deleteParameter } from './UtilityFunctions';

class ParameterTable extends React.Component {
    constructor(){
        super();
        this.state = {
           vat: 24,
           description: "",
           type: "kg",
           price: 0,
           params: []
        };
        this.columnLabels=[
            "Περιγραφή", "Τύπος", "Τιμή ανά μονάδα", ""
        ]
    }

    registerParameter() {
        registerParameter(this.state.description, this.state.type, this.state.price).then(result=>{
            getParameters().then(params=>{
                this.setState({params: params})
            })
        })
    }

    delParameter(id) {
        deleteParameter(id).then(result=>{
            getParameters().then(params=>{
                this.setState({params: params})
            })
        })
    }
    componentDidMount() {
        getParameters().then(params=>{
            this.setState({})
            getVat().then(vat=>{
                this.setState({
                    params: params,
                    vat:vat.value
                })
            })
        })
    }


    handleChange(e){
        if(e.target.id === 'type') {
            if(e.target.value === 'κιλό') {
                this.setState({[e.target.id]: 'kg'})
            } else if(e.target.value === 'τ.μ.') {
                this.setState({[e.target.id]: 'sm'})
            }  else if(e.target.value === 'τεμάχιο') {
                this.setState({[e.target.id]: 'piece'})
            }
        } else if(e.target.id === 'vat') {
            this.setState({vat: e.target.value})
            setVat(e.target.value)
        } else {
          this.setState({[e.target.id]: e.target.value})
        }
    }


    render() {
        const {params} = this.state
        return (
            <div>
                <h2>Φ.Π.Α.</h2>
                <Form.Control style={{width: "80px", margin: "auto", display: "block"}} id="vat" type="number" min={0} value={this.state.vat} onChange={(e) => this.handleChange(e)}/>
                <h2>Τύποι Προιόντων</h2>
                <Container fluid>
                    <Row key="title">
                        {
                            this.columnLabels.map(label=><Col key={label}><strong>{label}</strong></Col>)
                        }
                    </Row>
                    {
                        params.map((param, index)=>(
                            <Row key={index}>
                                <Col>{param.description}</Col>
                                <Col>{param.type}</Col>
                                <Col>{param.price}</Col>
                                <Col><Button variant="danger" onClick={this.delParameter.bind(this,param.id)}>Διαγραφή</Button></Col>
                            </Row>
                        ))
                    }
                    <Row>
                        <Col><Form.Control id="description" value={this.state.description} onChange={(e) => this.handleChange(e)}/></Col>
                        <Col>
                        <Form.Control id="type" onChange={(e) => this.handleChange(e)} as="select" custom>
                            <option>κιλό</option>
                            <option>τ.μ.</option>
                            <option>τεμάχιο</option>
                        </Form.Control>
                        </Col>
                        <Col><Form.Control id="price" type="number" value={this.state.price} onChange={(e) => this.handleChange(e)}/></Col>
                        <Col><Button onClick={this.registerParameter.bind(this)}>Καταχώρηση</Button></Col>
                    </Row>
                </Container>
            </div>

        )
  }
}

export default ParameterTable;
