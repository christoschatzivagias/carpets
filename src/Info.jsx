import React from 'react'
import { getYearReport } from './UtilityFunctions';

class Info extends React.Component {
    constructor(){
        super();
        this.state = {
            all: 0,
            delivered: 0,
            stored: 0
         };
    }

    componentDidMount() {
        getYearReport().then(report=>{
            this.setState({
                all: report.all,
                delivered: report.delivered,
                stored: report.stored
            })
        })
    }

    render() {
        const { all, delivered, stored } = this.state
        return (
            <div>
                <h2>Παραληφθέντα:{all}</h2>
                <h2>Αποθηκευμένα:{stored}</h2>
                <h2>Παραδομένα:{delivered}</h2>
            </div>
        )
    }
}

export default Info;
