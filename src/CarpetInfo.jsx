import React from 'react'
import { Row, Col, Container,Form, Button } from 'react-bootstrap';
import { getCarpetById, getCustomerById, getParameters, updateCarpet } from './UtilityFunctions'
import dateformat from "dateformat";
import ServerMessage from './ServerMessage'

class CarpetDeliveryTable extends React.Component {
    constructor(props){
        super();
        this.state = {
           carpetid: props.match.params.carpetid,
           customerid:"",
           customername: "",
           customersurname: "",
           customerstreet: "",
           customerfloor: "",
           customerregion: "",
           customertelephone: "",
           customercellphone: "",
           customerpostalcode: "",
           typedescription: "",
           shelvechar: 'Α',
           shelvenum: 10,
           pickupid: "",
           pickupdate: "",
           deliverid: "",
           deliverdate: "",
           priceperunit:"",
           length:1,
           width:1,
           weight:1,
           extent:1,
           price: 0,
           extraprice: 0,
           pricevat: 0,
           notes: "",
           carpettypes: [],
           vat:24,
           status:0,

        };

        this.shelvechars = ['Α','Β','Γ','Δ','Ε','Ζ','Η','Θ','Ζ','Ι','Κ','Λ','Μ','Ν','Ξ','Ο','Π','Ρ','Σ','Υ','Φ','Χ','Ψ','Ω']

    }


    update() {
  
        updateCarpet(
            this.getCurrentTypePrice(this.state.typedescription),
            this.getCurrentType(this.state.typedescription),
            this.state.typedescription,
            this.state.carpetid,
            this.state.customerid, 
            this.state.length,
            this.state.width,
            this.state.weight,
            this.state.extent,
            this.state.price,
            this.state.pricevat,
            this.state.extraprice,
            this.state.notes,
            this.state.pickupid, 
            this.state.pickupdate,
            null,
            null,
            this.state.shelvechar + this.state.shelvenum.toString())
        .then(status => {
            this.setState({
                status: status
            })
        })
    }

    submitButtonActive() {
        return this.state.typedescription.length > 0;
    }

    handleChange(e){
        if(e.target.id === 'typedescription') {
            const type = this.getTypeByDesc(e.target.value)
            const price =  this.getCurrentTypePriceByDesc(e.target.value)
            if(type === 'sm') {
                this.setState({
                    typedescription:e.target.value,
                    priceperunit: price,
                    pricetype: type,
                    price: this.state.length * this.state.width * price + this.state.extraprice, 
                    pricevat: (this.state.length * this.state.width * price + this.state.extraprice) * (1 + this.state.vat/100)
                })
            } else if(type === 'kg') {
                this.setState({
                    typedescription:e.target.value,
                    priceperunit: price,
                    pricetype: type,
                    price: this.state.weight * price + this.state.extraprice,
                    pricevat: (this.state.weight * price + this.state.extraprice) * (1 + this.state.vat/100)

                }) 
            } else if(type === 'piece') {
                this.setState({
                    typedescription:e.target.value,
                    priceperunit: price,
                    pricetype: type,
                    price: price + this.state.extraprice, 
                    pricevat: (price + this.state.extraprice) * (1 + this.state.vat/100)
                }) 
            }
        } else if(e.target.id === 'extraprice') {
            this.setState({
                extraprice: parseFloat(e.target.value)
            })
            const type = this.getTypeByDesc(this.state.typedescription)
            const price =  this.getCurrentTypePriceByDesc(this.state.typedescription)

            if(type === 'sm') {
                this.setState({
                    price: this.state.length * this.state.width * price + parseFloat(e.target.value),
                    pricevat: (this.state.length * this.state.width * price + parseFloat(e.target.value)) * (1 + this.state.vat/100)
                })
            } else if(type === 'kg') {
                this.setState({
                    price: this.state.weight * price + parseFloat(e.target.value), 
                    pricevat: (this.state.weight * price + parseFloat(e.target.value)) * (1 + this.state.vat/100)

                }) 
            } else if(type === 'piece') {
                this.setState({
                    price: price + parseFloat(e.target.value), 
                    pricevat: (price + parseFloat(e.target.value)) * (1 + this.state.vat/100)
                }) 
            }
          
        } else if(e.target.id === 'length') {
            if(this.getCurrentType(this.state.typedescription) === 'sm') {
                this.setState({
                    length: e.target.value,
                    extent: parseFloat(e.target.value) * this.state.width,
                    price: parseFloat(e.target.value) * this.state.width * this.getCurrentTypePrice(this.state.typedescription) + this.state.extraprice,
                    pricevat: parseFloat(e.target.value) * this.state.width * this.getCurrentTypePrice(this.state.typedescription) * (1 + this.state.vat/100)  + this.state.extraprice
                }) 
            }
        } else if(e.target.id === 'width') {
            if(this.getCurrentType(this.state.typedescription) === 'sm') {
                this.setState({
                    width: e.target.value,
                    extent: parseFloat(e.target.value) * this.state.length,
                    price: parseFloat(e.target.value) * this.state.length * this.getCurrentTypePrice(this.state.typedescription) + this.state.extraprice, 
                    pricevat: parseFloat(e.target.value) * this.state.length * this.getCurrentTypePrice(this.state.typedescription) * (1 + this.state.vat/100) + this.state.extraprice
                })
            }
        } else if(e.target.id === 'weight') {
            if(this.getCurrentType(this.state.typedescription) === 'kg') {
                this.setState({
                    weight: e.target.value,
                    price: parseFloat(e.target.value) * this.getCurrentTypePrice(this.state.typedescription) + this.state.extraprice,
                    pricevat: parseFloat(e.target.value) * this.getCurrentTypePrice(this.state.typedescription) * (1 + this.state.vat/100) + this.state.extraprice
                })
            }
        } else {
            this.setState({
                [e.target.id]: e.target.value,
            })    
        }
    }

   

    componentDidMount() {
        getParameters().then(params=>{
            this.setState({
                carpettypes:params
            })
            getCarpetById(this.state.carpetid).then(carpet=>{
                this.setState({
              
                    pickupid: carpet.pickupid,
                    pickupdate: carpet.pickupdate,
                    deliverid: carpet.deliverid,
                    deliverdate: carpet.deliverdate,
                    length: carpet.length,
                    width: carpet.width,
                    weight: carpet.weight,
                    extent: carpet.extent,
                    typedescription: carpet.typedescription,
                    priceperunit: carpet.priceperunit,
                    pricetype: carpet.pricetype,
                    price: carpet.price,
                    extraprice: carpet.extraprice,
                    pricevat: carpet.pricevat,
                    notes: carpet.notes,
                    shelve: carpet.shelve
                })
                getCustomerById(carpet.customerid).then(customer=>{
                    this.setState({
                        customerid: customer.id,
                        customername: customer.name,
                        customersurname: customer.surname,
                        customerstreet: customer.street,
                        customerfloor: customer.floor,
                        customerregion: customer.region,
                        customertelephone: customer.telephone,
                        customercellphone: customer.cellphone,
                        customerpostalcode: customer.postalcode,
                    })
                })
            }).catch(error=>{
                console.log("Error" + error)
            })
        })
      
    }


    getCurrentType(description) {
        const type = this.state.carpettypes.find(carpettype=>carpettype.description===description)
        if(type === undefined)
            return this.state.carpettypes[0].type;
        return type.type
    }

    getTypeByDesc(description) {
        const type = this.state.carpettypes.find(carpettype=>carpettype.description===description)
        if(type === undefined)
            return undefined;
        return type.type
    }

    getCurrentTypePrice(description) {
        const type = this.state.carpettypes.find(carpettype=>carpettype.description===description)
        if(type === undefined)
            return this.state.carpettypes[0].price;
        return type.price
    }

    getCurrentTypePriceByDesc(description) {
        const type = this.state.carpettypes.find(carpettype=>carpettype.description===description)
        if(type === undefined)
            return undefined
        return type.price
    }


    render() {
        const {
            customername,
            customersurname,
            customerstreet,
            customerfloor,
            customerregion,
            customertelephone,
            customercellphone,
            customerpostalcode,
            typedescription,
            length,
            width,
            weight,
            price,
            extraprice,
            pricetype,
            pricevat,
            shelvechar,
            shelvenum,
            pickupid,
            pickupdate,
            deliverid,
            deliverdate,
            carpettypes,
            extent,
        } = this.state
        return (
            <div>
                {
                    pickupid === undefined && <div><h2>Μη έγκυρος κωδικός χαλιού</h2></div>
                }
                {

                pickupid !== undefined && 
                    <Container>
                        <Row>
                            <Col xs={4}><h3>Όνομα</h3></Col>
                            <Col xs={4}><h3>{customersurname} {customername}</h3></Col>
                        </Row>
                        <Row>
                            <Col xs={4}><h3>Διεύθυνση</h3></Col>
                            <Col xs={4}><h3>{customerstreet} {customerfloor} {customerregion} {customerpostalcode}</h3></Col>
                        </Row>
                        <Row>
                            <Col xs={4}><h3>Κινητό</h3></Col>
                            <Col xs={4}><h3>{customercellphone}</h3></Col>
                        </Row>
                        <Row>
                            <Col xs={4}><h3>Σταθερό</h3></Col>
                            <Col xs={4}><h3>{customertelephone}</h3></Col>
                        </Row>
                        <Row>
                            <Col xs={4}><h3>Ράφι</h3></Col>
                            <Col xs={4}>
                            <Form.Control disabled={deliverid !== null} style={{fontSize: 26,textAlignLast:"center"}} id="shelvechar" value={shelvechar} onChange={(e) => this.handleChange(e)} as="select" custom>
                                {
                                    this.shelvechars.map((shelvechar,index)=>(
                                        <option key={index}>{shelvechar}</option>
                                    ))
                                }
                            </Form.Control>
                            <Form.Control disabled={deliverid !== null} style={{fontSize: 26,textAlign:"center"}} id="shelvenum" type="number" min={10} max={31} value={shelvenum} onChange={(e) => this.handleChange(e)}/>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={4}><h3>Αρ.Παραλ</h3></Col>
                            <Col xs={4}><h3>{pickupid}</h3></Col>
                        </Row>
                        <Row>
                            <Col xs={4}><h3>Ημ.Παραλ</h3></Col>
                            <Col xs={4}><h3>{dateformat(new Date(deliverdate!=="" ? pickupdate : null), "dd/mm/yyyy")}</h3></Col>
                        </Row>
                        <Row>
                            <Col xs={4}><h3>Τύπος Χαλιού</h3></Col>
                            <Col xs={4}>
                                <Form.Control disabled={deliverid !== null} style={{fontSize: 26,textAlignLast:"center"}} id="typedescription" value={typedescription} onChange={(e) => this.handleChange(e)} as="select" custom>
                                {
                                    carpettypes.map((carpettype,index)=>(
                                        <option key={index}>{carpettype.description}</option>
                                    ))
                                }
                                </Form.Control>
                            </Col>
                        </Row>
                        {
                            pricetype === 'sm' &&
                            <Row>
                                <Col xs={4}><h3>Μήκος</h3></Col>
                                <Col xs={4}><Form.Control disabled={deliverid !== null} style={{fontSize: 26,textAlign:"center"}} id="length" type="number" min={0} value={length} onChange={(e) => this.handleChange(e)}/></Col>
                            </Row>
                        }
                        {
                            pricetype === 'sm' &&
                            <Row>
                                <Col xs={4}><h3>Πλάτος</h3></Col>
                                <Col xs={4}><Form.Control disabled={deliverid !== null} style={{fontSize: 26,textAlign:"center"}} id="width" type="number" min={0} value={width} onChange={(e) => this.handleChange(e)}/></Col>
                            </Row>
                        }
                        {
                            pricetype === 'kg' &&
                            <Row>
                                <Col xs={4}><h3>Βάρος</h3></Col>
                                <Col xs={4}><Form.Control disabled={deliverid !== null} style={{fontSize: 26,textAlign:"center"}} id="weight" type="number" min={0} value={weight} onChange={(e) => this.handleChange(e)}/></Col>
                            </Row>
                        }
                        {
                            pricetype === 'sm' &&
                            <Row>
                                <Col xs={4}><h3>Τ.Μ.</h3></Col>
                                <Col xs={4}><Form.Control style={{fontSize: 26,textAlign:"center"}} id="extent" disabled type="number" min={0} value={extent} onChange={(e) => this.handleChange(e)}/></Col>
                            </Row>
                        }
                        <Row>
                            <Col xs={4}><h3>Έξτρα</h3></Col>
                            <Col xs={4}><Form.Control disabled={deliverid !== null} style={{fontSize: 26,textAlign:"center"}} id="extraprice" type="number" min={0} value={extraprice.toFixed(2)} onChange={(e) => this.handleChange(e)}/></Col>
                        </Row>
                        <Row>
                            <Col xs={4}><h3>Τιμή</h3></Col>
                            <Col xs={4}><Form.Control style={{fontSize: 26,textAlign:"center"}} id="price" disabled type="number" min={0} value={price.toFixed(2)} onChange={(e) => this.handleChange(e)}/></Col>
                        </Row>
                        
                        <Row>
                            <Col xs={4}><h3>Τιμή με ΦΠΑ</h3></Col>
                            <Col xs={4}><Form.Control style={{fontSize: 26,textAlign:"center"}} id="pricevat" disabled min={0} value={pricevat.toFixed(2)} onChange={(e) => this.handleChange(e)}/></Col>
                        </Row>                
                        <Row>
                            <Col xs={4}><h3>Αρ.Παραδ</h3></Col>
                            <Col xs={4}><h3>{deliverid}</h3></Col>
                        </Row>
                        <Row>
                            <Col xs={4}><h3>Ημ.Παραδ</h3></Col>
                            <Col xs={4}><h3>{deliverdate !== null && deliverdate !== "" ? dateformat(new Date(deliverdate), "dd/mm/yyyy") : ""}</h3></Col>
                        </Row>
                        <Row>
                            <Col>
                            <Button style={{
                                    float: "right",
                                    margin: "15px"
                                }}
                                
                                onClick={()=>{this.update()}}
                                disabled={!this.submitButtonActive()}

                                >Ενημέρωση</Button>
                            </Col>
                            <Col />
                        </Row>
                    
                    </Container>
                }

                <ServerMessage show={this.state.status === 200} redirect={'/'} title='Επιτυχής!' message='Το προιόν ενημερώθηκε επιτυχώς!'/>
                <ServerMessage show={this.state.status === 404} redirect={'/'} title='Λάθος!' message='Το προιόν δεν ενημερώθηκε!'/>


            </div>

        )
  }
}

export default CarpetDeliveryTable;
