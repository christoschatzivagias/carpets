var express = require('express');
var app = express();
var Sequelize = require("sequelize");
var bodyParser = require('body-parser');

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    next();
});

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const connection = new Sequelize('db', 'user', 'pass', {
  host: 'localhost',
  dialect: 'sqlite',
  storage: 'db.sqlite',
  operatorsAliases: false
})


connection.sync({
  logging: console.log
})
.then(() => {
  console.log('Connection to database established successfully.');
})
.catch(err => {
  console.log('Unable to connect to the database: ', err);
})

const Customer = connection.define('Customers', {
  id: {
       type: Sequelize.INTEGER,
       primaryKey: true
  },
  name: Sequelize.STRING,
  surname: Sequelize.STRING,
  street: Sequelize.STRING,
  floor: Sequelize.NUMBER,
  region: Sequelize.STRING,
  postalcode: Sequelize.NUMBER,
  telephone: Sequelize.NUMBER,
  cellphone: Sequelize.NUMBER,
  cellphone2: Sequelize.NUMBER,
  vatid:Sequelize.NUMBER,
  profession: Sequelize.STRING,
  taxregion: Sequelize.STRING
})


const Carpet = connection.define('Carpets', {
  id: {
       type: Sequelize.INTEGER,
       primaryKey: true
  },
  priceperunit: Sequelize.NUMBER,
  pricetype: Sequelize.STRING,
  typedescription: Sequelize.STRING,
  customerid: Sequelize.INTEGER,
  length: Sequelize.NUMBER,
  width: Sequelize.NUMBER,
  weight: Sequelize.NUMBER,
  extent: Sequelize.NUMBER,
  price: Sequelize.NUMBER,
  extraprice: Sequelize.NUMBER,
  pricevat: Sequelize.NUMBER,
  notes:  Sequelize.STRING,
  shelve: Sequelize.STRING,
  pickupid:Sequelize.INTEGER,
  pickupdate: Sequelize.DATE,
  deliverid:Sequelize.INTEGER,
  deliverdate: Sequelize.DATE,
})

const Config = connection.define('Configs', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  name: {
    type: Sequelize.STRING,
    unique: true
  },
  value: Sequelize.STRING
})

Config.sync().then(async() => {
  const vatrec = await Config.findOne({
    where :{
      name: "vat"
    }
  }).then(vatrec=>{
    if(vatrec == null) {
      Config.create({
        name: 'vat',
        value: 24
      });
    }
  });

  
 
});

const Parameter = connection.define('Parameters', {
  id: {
       type: Sequelize.INTEGER,
       primaryKey: true
  },
  description: {
    type: Sequelize.STRING,
    unique: true
  },
  type: {
    type: Sequelize.ENUM,
    values: ['kg', 'sm', 'piece']
  },
  price: Sequelize.NUMBER

})

Customer.hasMany(Carpet, {
  foreignKey: {
    name: 'customerid',
    allowNull: false
  }
})

app.post('/api/v1/customer/register', (req, res) => {

  Customer.create({
    name: req.body.name,
    surname: req.body.surname,
    street: req.body.street,
    floor: req.body.floor,
    region: req.body.region,
    postalcode: req.body.postalcode,
    telephone: req.body.telephone,
    cellphone: req.body.cellphone,
    cellphone2: req.body.cellphone2,
    vatid: req.body.vatid,
    profession: req.body.profession,
    taxregion: req.body.taxregion})
    .then(result => {
    res.status(201).send("Success")
  }).catch(error=>{
    res.status(403).send("Error:" + error)
  });

});





app.post('/api/v1/carpet/register', (req, res) => {

  Carpet.create({
    id: req.body.id,
    priceperunit: req.body.priceperunit,
    pricetype: req.body.pricetype,
    typedescription: req.body.typedescription,
    customerid: req.body.customerid,
    length: req.body.length,
    width: req.body.width,
    weight: req.body.weight,
    extent: req.body.extent,
    pieces: req.body.pieces,
    price: req.body.price,
    extraprice: req.body.extraprice,
    pricevat: req.body.pricevat,
    notes: req.body.notes,
    pickupid: req.body.pickupid,
    pickupdate: req.body.pickupdate,
    deliverid: req.body.deliverid,
    deliverdate: req.body.deliverdate
  })
  .then(result => {
    res.status(201).send("Success")
  }).catch(error=>{
    console.error(error);
    res.status(403).send("Error:" + error)
  });
});


app.post('/api/v1/carpet/update', (req, res) => {
  Carpet.findOne({where:{id: req.body.id}}).then(async (carpet) => {
    if (carpet == null) 
      res.status(400).send("error");
    carpet.priceperunit = req.body.priceperunit;
    carpet.pricetype = req.body.pricetype;
    carpet.typedescription = req.body.typedescription;
    carpet.length = req.body.length;
    carpet.width = req.body.width;
    carpet.weight = req.body.weight;
    carpet.extent = req.body.extent;
    carpet.pieces = req.body.pieces;
    carpet.price = req.body.price;
    carpet.extraprice = req.body.extraprice;
    carpet.pricevat = req.body.pricevat;
    carpet.notes = req.body.notes;
    carpet.pickupid = req.body.pickupid;
    carpet.pickupdate = req.body.pickupdate;
    carpet.deliverid = req.body.deliverid;
    carpet.deliverdate = req.body.deliverdate;
    carpet.shelve = req.body.shelve;
    carpet.save();
    res.status(200).send("OK");
  }).catch(error => {
    console.error(error);
    res.status(404).send(error);
  });
});



app.post('/api/v1/carpet/deliver/:id', (req, res) => {

  Carpet.findOne({where:{id: req.params.id}}).then(async (carpet) => {
    if (carpet == null) 
      res.status(400).send("ERROR");
    carpet.deliverid = req.body.deliverid;
    carpet.deliverdate = req.body.deliverdate;
    carpet.save();
    res.status(200).send("OK");
  }).catch(error => {
    console.error(error);
    res.status(404).send(error);
  });
});

app.post('/api/v1/carpet/delete/:id', (req, res) => {

  Carpet.findOne({where:{id: req.params.id}}).then(async (carpet) => {
    if (carpet == null) {
      res.status(400).send("ERROR");
    } else {
      await carpet.destroy();
      res.status(200).send("OK");
    }
   
  }).catch(error => {
    console.error(error);
    res.status(404).send(error);
  });
});


app.post('/api/v1/customer/delete/:id', (req, res) => {

  Customer.findOne({where:{id: req.params.id}}).then(async (cust) => {
    if (cust == null) {
      res.status(400).send("ERROR");
    } else {
      await cust.destroy()
      res.status(200).send("OK");
    }
  
  }).catch(error => {
    console.error(error);
    res.status(404).send(error);
  });
});


app.post('/api/v1/parameter/register', (req, res) => {

  Parameter.create({
    description: req.body.description,
    type: req.body.type,
    price: req.body.price
  })
  .then(result => {
    res.status(201).send("Success")
  }).catch(error=>{
    console.error(error);
    res.status(403).send(error)
  });
});


app.get('/api/v1/parameter/get', (req, res) => {
  Parameter.findAll()
  .then(params => {
    res.status(200).send(params);
  }).catch(error => {
    console.error(error);
    res.status(404).send(error);
  })
});

app.post('/api/v1/parameter/delete/:paramid', (req, res) => {
  Parameter.findOne({
    where:{
      id: req.params.paramid
      },
  }).then(async (param) => {
    if(param === null)
      res.status(400).send("ERROR");
    param.destroy().then(result=>{
      res.status(200).send("OK");
    }).catch(error => {
      console.error(error);
      res.status(404).send(error);
    })
  }).catch(error => {
    console.error(error);
    res.status(404).send(error);
  })
});


app.get('/api/v1/customer/get/:customerid', (req, res) => {
  Customer.findOne({
      where:{
        id: req.params.customerid
        },
  }).then(customer => {
    res.status(200).send(customer);
  }).catch(error => {
    console.error(error);
    res.status(404).send(error);
  })
});

app.get('/api/v1/carpet/getbycustomer/:customerid', (req, res) => {
  Carpet.findAll({
      where:{
        customerid: req.params.customerid
        },
  }).then(carpets => {
    res.status(200).send(carpets);
  }).catch(error => {
    console.error(error);
    res.status(404).send(error);
  })
});


app.get('/api/v1/carpet/getbyid/:id', (req, res) => {
  Carpet.findOne({
      where:{
        id: req.params.id
        },
  }).then(carpet => {
    res.status(200).send(carpet);
  }).catch(error => {
    console.error(error);
    res.status(404).send(error);
  })
});


app.get('/api/v1/vat', (req, res) => {
  Config.findOne({
      where:{
        name: "vat"
        },
  }).then(vat => {
    res.status(200).send(vat);
  }).catch(error => {
    console.error(error);
    res.status(404).send(error);
  })
});

app.post('/api/v1/vat', (req, res) => {
  Config.findOne({
      where:{
        name: "vat"
        },
  }).then(vat => {
    vat.value = req.body.vat;
    vat.save();
    res.status(200).send("ok");
  }).catch(error => {
    console.error(error);
    res.status(404).send(error);
  })
});


app.get('/api/v1/shelve/get/:shelveid', (req, res) => {
  Carpet.findAll({
      where:{
        shelve: req.params.shelveid
        },
  }).then(carpets => {
    res.status(200).send(carpets);
  }).catch(error => {
    console.error(error);
    res.status(404).send(error);
  })
});



app.get('/api/v1/customers', function(req, res) {
  Customer.findAll().then(customers => {
    res.status(200).send(customers);
  }).catch(error => {
    console.error(error);
    res.status(404).send(error);
  });
});


app.get('/api/v1/search/:query', function(req, res) {

  const queryList = req.params.query.split('+')

  const query = queryList.map(query=>{
    return {
      [Sequelize.Op.or]:{
        name: {
            [Sequelize.Op.like]: '%' + query + '%'
        },
        surname: {
            [Sequelize.Op.like]: '%' + query + '%'
        },
        telephone: {
            [Sequelize.Op.like]: '%' + query + '%'
        },
        cellphone: {
            [Sequelize.Op.like]: '%' + query + '%'
        },
        vatid: {
          [Sequelize.Op.like]: '%' + query + '%'
      },
      }
    }

  })

  Customer.findAll({
    where: {
        [Sequelize.Op.or]:query
    }
  }).then(customers => {
    res.status(200).send(customers);
  }).catch(error => {
    console.error(error);
    res.status(404).send(error);
  });
});


app.get('/api/v1/report/:year', function(req, res) {
  const startYear = req.params.year
  const endYear = req.params.year + 1

  Carpet.findAll({
    where: {
      createdAt: {
        [Sequelize.Op.between]: [startYear + "-01-01", endYear + "-01-01"]
      }
    }
  }).then(carpets => {
    const delivered = carpets.filter(carpet=>carpet.deliverid!==null)
    res.send({all:carpets.length, delivered: delivered.length, stored: carpets.length - delivered.length});
  }).catch(error => {
    console.error(error);
    res.status(404).send(error);
  });;
})

app.get('/api/v1/stored', function(req, res) {
  Carpet.findAll({
    where: {
      deliverid: ""
    }
  }).then(carpets => {
    res.send(carpets);
  }).catch(error => {
    console.error(error);
    res.status(404).send(error);
  });;
})

app.listen(8080);
console.log('Listening...');
